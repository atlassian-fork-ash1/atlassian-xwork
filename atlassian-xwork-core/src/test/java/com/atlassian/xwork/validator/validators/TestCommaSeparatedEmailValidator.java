package com.atlassian.xwork.validator.validators;

import com.opensymphony.xwork.LocaleProvider;
import com.opensymphony.xwork.TextProviderSupport;
import com.opensymphony.xwork.ValidationAwareSupport;
import com.opensymphony.xwork.validator.DelegatingValidatorContext;
import com.opensymphony.xwork.validator.ValidatorContext;
import org.jmock.MockObjectTestCase;

import java.util.List;
import java.util.Locale;

public class TestCommaSeparatedEmailValidator extends MockObjectTestCase {
    private ValidatorContext validatorContext;
    private CommaSeparatedEmailValidator validator;

    protected void setUp() throws Exception {
        super.setUp();
        LocaleProvider localeProvider = new LocaleProvider() {
            public Locale getLocale() {
                return Locale.getDefault();
            }
        };
        validatorContext = new DelegatingValidatorContext(new ValidationAwareSupport(),
                new TextProviderSupport(getClass(), localeProvider), localeProvider);
        validator = new CommaSeparatedEmailValidator();
        validator.setValidatorContext(validatorContext);
        validator.setFieldName("email");
        validator.setDefaultMessage("invalid");
    }

    protected static void assertSingletonList(Object expected, List actual) {
        assertEquals("List has more than one item: " + actual.toString(), 1, actual.size());
        assertEquals(expected, actual.get(0));
    }

    public void testSingleEmailIsValid() throws Exception {
        validator.validate(new EmailBean("admin@example.org"));
        assertFalse(validatorContext.hasActionErrors());
        assertFalse(validatorContext.hasFieldErrors());
    }

    public void testNonEmailIsInvalid() throws Exception {
        validator.validate(new EmailBean("notanemail"));
        assertFalse(validatorContext.hasActionErrors());
        assertTrue(validatorContext.hasFieldErrors());
        assertSingletonList("invalid", (List) validatorContext.getFieldErrors().get("email"));
    }

    public void testMultipleNonEmailIsInvalid() throws Exception {
        validator.validate(new EmailBean("notanemail, stillnotanemail"));
        assertFalse(validatorContext.hasActionErrors());
        assertTrue(validatorContext.hasFieldErrors());
        assertSingletonList("invalid", (List) validatorContext.getFieldErrors().get("email"));
    }

    public void testEmptyStringIsValid() throws Exception {
        validator.validate(new EmailBean(""));
        assertFalse(validatorContext.hasActionErrors());
        assertFalse(validatorContext.hasFieldErrors());
    }

    public void testNullIsValid() throws Exception {
        validator.validate(new EmailBean(null));
        assertFalse(validatorContext.hasActionErrors());
        assertFalse(validatorContext.hasFieldErrors());
    }

    public void testMultipleEmailsAreValid() throws Exception {
        validator.validate(new EmailBean("admin@example.org, user@example.org"));
        assertFalse(validatorContext.hasActionErrors());
        assertFalse(validatorContext.hasFieldErrors());
    }

    public void testMultipleMixedAreInvalid() throws Exception {
        validator.validate(new EmailBean("admin@example.org, notanemail, user@example.org"));
        assertFalse(validatorContext.hasActionErrors());
        assertTrue(validatorContext.hasFieldErrors());
        assertSingletonList("invalid", (List) validatorContext.getFieldErrors().get("email"));
    }

    private static class EmailBean {
        private final String email;

        private EmailBean(String email) {
            this.email = email;
        }

        public String getEmail() {
            return email;
        }
    }
}
