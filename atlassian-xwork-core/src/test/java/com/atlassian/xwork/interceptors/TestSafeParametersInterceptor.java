package com.atlassian.xwork.interceptors;

import com.atlassian.xwork.ParameterSafe;
import com.opensymphony.xwork.ActionSupport;
import junit.framework.TestCase;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class TestSafeParametersInterceptor extends TestCase {
    @ParameterSafe
    public static class SafeReturnType {
    }

    public static class UnsafeReturnType {
    }

    public static class MyAction extends ActionSupport {
        public void setWhiteCssClass(String css) {
        }

        public void setCssClass(String css) {
        }

        public void setFoo(String foo) {
        }

        public void setBar(String bar) {
        }

        public void setFoo_bar(String foo_bar) {
        }

        public List getBaz() {
            return Collections.EMPTY_LIST;
        }

        @ParameterSafe
        public List getQuux() {
            return Collections.EMPTY_LIST;
        }

        public SafeReturnType getSafeReturnType() {
            return new SafeReturnType();
        }

        public UnsafeReturnType getUnsafeReturnType() {
            return new UnsafeReturnType();
        }

        @ParameterSafe
        public String getWhiteCssClass(String css) {
            return "css";
        }

        public String getCssClass(String css) {
            return "css";
        }

        public String getFoo() {
            return "foo";
        }

        @ParameterSafe
        public String getBar() {
            return "bar";
        }

        @ParameterSafe
        public Map getBarMap() {
            return Collections.emptyMap();
        }

        @ParameterSafe
        public Map getBar_Map() {
            return Collections.emptyMap();
        }

        public Map getFooMap() {
            return Collections.emptyMap();
        }

        public Map getFoo_Map() {
            return Collections.emptyMap();
        }
    }

    /**
     * Tests: CONF-33480, classloader manipulation hack, as fixed in Struts2 2.3.16.2.
     * where a parameter name includes class.classLoader type access
     * eg. http://localhost:8090/searchsite.action?searchQuery.class.classLoader.resources.dirContext.docBase=/new-doc-base
     */
    public void testClassLoaderManipulation() {
        // Simple access (with and without a white list)
        assertTrue(isSafe("bar"));
        assertTrue(isSafe("foo"));
        assertTrue(isSafe("whiteCssClass"));
        assertTrue(isSafe("CssClass"));
        assertTrue(isSafe("class"));
        assertTrue(isSafe("bar", true));
        assertTrue(isSafe("foo", true));
        assertTrue(isSafe("whiteCssClass", true));
        assertTrue(isSafe("CssClass", true));
        assertTrue(isSafe("class", true));

        // Complex access allowed with or without white list, as long as 'class' not followed by special chars
        assertTrue(isSafe("bar.object"));
        assertTrue(isSafe("bar.class"));
        assertTrue(isSafe("bar.two.three"));
        assertTrue(isSafe("one.two.three", true));
        assertTrue(isSafe("foo.cssClass", true));
        assertTrue(isSafe("bar.cssClass", true));
        assertTrue(isSafe("foo.classLoader", true));

        // Complex access blocked if not on white list or 'class' followed by special characters
        assertFalse(isSafe("foo.object"));
        assertFalse(isSafe("foo.class"));
        assertFalse(isSafe("whiteCssClass.object"));
        assertFalse(isSafe("cssClass.object"));
        assertFalse(isSafe("whiteCssClass.object", true));
        assertFalse(isSafe("cssClass.object", true));
        assertFalse(isSafe("foo['cl' + 'ass']", true));
        assertFalse(isSafe("foo.class.bad", true));
        assertFalse(isSafe("foo.class.classLoader", true));
        assertFalse(isSafe("foo.class.classLoader.resources.dirContext.docBase", true));
        assertFalse(isSafe("foo.Class.classLoader.resources.dirContext.docBase", true));
        assertFalse(isSafe("foo.Class.classLoader.jarPath", true));
        assertFalse(isSafe("foo.['Class']['classLoader']['jarPath']", true));
        assertFalse(isSafe("foo.[\"Class\"]['classLoader']['jarPath']", true));
        assertFalse(isSafe("foo.['c'+'lass'].classLoader.jarPath", true));
        assertFalse(isSafe("foo.Class['classLoader']['jarPath']", true));
        assertFalse(isSafe("foo.Class[\"classLoader\"]['jarPath']", true));
        assertFalse(isSafe("foo.Class[\"classLoader\"]['jarPath']", true));
        assertFalse(isSafe("foo.Class[\"classLoader\"]['jarPath']", true));

        // We now filter param[previousparam] syntax where a parameter name may be resolved using an existing value
        assertTrue(isSafe("bar[0]", true));
        assertTrue(isSafe("bar['bar']", true));
        assertTrue(isSafe("colourMap['property.style.topbarcolour']", true));

        assertFalse(isSafe("bar[bad]", true));
        assertFalse(isSafe("bar.ok[bad]", true));
        assertFalse(isSafe("bar(0)", true));
        assertFalse(isSafe("bar('bar')", true));
        assertFalse(isSafe("bar.['hide'].bad[stuff]", true));

        // Additional random tests
        assertFalse(isSafe("model. class.classLoader", true));
        assertFalse(isSafe("model.class .classLoader", true));

        assertFalse(isSafe("bar.['hide'].bad[stuff]", true));
        assertFalse(isSafe("bar.hide.bad[stuff]", true));
        assertFalse(isSafe("bar[0].bad[stuff]", true));

        assertFalse(isSafe("actionErrors", true));
        assertFalse(isSafe("actionMessages", true));
    }


    public void testSimpleSetters() {
        assertTrue(isSafe("foo"));
        assertTrue(isSafe("bar"));
        assertTrue(isSafe("foo_bar"));
    }

    public void testIndexedSetters() {
        assertTrue(isSafe("baz[0]"));
        assertTrue(isSafe("baz[1]"));
        assertTrue(isSafe("baz[2]"));
    }

    public void testTraverseIndexedSetters() {
        assertFalse(isSafe("baz[1].foo"));
        assertTrue(isSafe("baz[1].foo", true));
        assertTrue(isSafe("quux[1].foo"));
    }

    public void testMultipleTraversing() {
        assertFalse(isSafe("foo.bar.baz"));
        assertTrue(isSafe("foo.bar.baz", true));
        assertFalse(isSafe("unsafeReturnType.foo"));
        assertTrue(isSafe("unsafeReturnType.foo", true));
        assertTrue(isSafe("bar.bar.baz"));
        assertTrue(isSafe("safeReturnType.foo"));
    }

    public void testMixedTraversingAndIndexing() {
        assertFalse(isSafe("foo.bar[1]"));
        assertTrue(isSafe("foo.bar[1]", true));
        assertTrue(isSafe("bar.foo[1]"));
    }

    public void testUnsafeCharacters() {
        assertFalse(isSafe("foo#bar"));
        assertFalse(isSafe("foo#bar", true));
        // nasty unicode
        assertFalse(isSafe("foo\u0023bar"));
        assertFalse(isSafe("foo\u0023bar", true));
        // escaped nasty unicode
        assertFalse(isSafe("foo\\u0023bar"));
        assertFalse(isSafe("foo\\u0023bar", true));
    }

    public void testDotNotation() {
        assertTrue(isSafe("bar.property"));
        assertTrue(isSafe("safeReturnType.property"));
        assertFalse(isSafe("foo.property"));
        assertTrue(isSafe("foo.property", true));
        assertFalse(isSafe("unsafeReturnType.property"));
        assertTrue(isSafe("unsafeReturnType.property", true));
    }

    public void testMapNotation() {
        assertTrue(isSafe("barMap['key']"));
        assertTrue(isSafe("bar_Map['key']"));
        assertTrue(isSafe("bar_Map['key_key']"));
        assertFalse(isSafe("fooMap['key']"));
        assertTrue(isSafe("fooMap['key']", true));
        assertFalse(isSafe("foo_Map['key']"));
        assertTrue(isSafe("foo_Map['key']", true));
        assertFalse(isSafe("foo_Map['key_key']"));
        assertTrue(isSafe("foo_Map['key_key']", true));
    }

    public void testMapNotationWithBadCharacters() {
        assertFalse(isSafe("barMap['key#G']"));
        assertFalse(isSafe("barMap['key#G']", true));
        assertFalse(isSafe("barMap['key!G']"));
        assertFalse(isSafe("barMap['key!G']", true));
        assertFalse(isSafe("barMap['key\u0023G']"));
        assertFalse(isSafe("barMap['key\u0023G']", true));
    }

    private boolean isSafe(String parameter) {
        return isSafe(parameter, false);
    }

    private boolean isSafe(String parameter, boolean disableAnnotationChecks) {
        return SafeParametersInterceptor.isSafeParameterName(parameter, new MyAction(), disableAnnotationChecks);
    }
}
