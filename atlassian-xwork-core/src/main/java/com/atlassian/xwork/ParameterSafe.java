package com.atlassian.xwork;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a class as being safe for use as a complex form parameter. By marking a class with this
 * interceptor you are guaranteeing that there aren't any dangerous setters or getters that may be exposed
 * to the XWork parameters interceptor
 *
 * @see com.atlassian.xwork.interceptors.SafeParametersInterceptor
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface ParameterSafe {

}
