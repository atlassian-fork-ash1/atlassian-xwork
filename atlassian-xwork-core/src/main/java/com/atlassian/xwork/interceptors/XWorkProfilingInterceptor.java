package com.atlassian.xwork.interceptors;

import com.atlassian.util.profiling.ProfilingUtils;
import com.atlassian.util.profiling.UtilTimerStack;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.ActionInvocation;
import com.opensymphony.xwork.ActionProxy;
import com.opensymphony.xwork.interceptor.AroundInterceptor;

public class XWorkProfilingInterceptor extends AroundInterceptor {
    String location;

    protected void before(ActionInvocation actionInvocation) throws Exception {
        UtilTimerStack.push(makeStackKey(actionInvocation.getProxy()));
        ServletActionContext.getRequest(); // needed to make sure the request is present (I think)
    }

    protected void after(ActionInvocation actionInvocation, String string) throws Exception {
        UtilTimerStack.pop(makeStackKey(actionInvocation.getProxy()));
    }

    private String makeStackKey(ActionProxy proxy) {
        String methodName = proxy.getConfig().getMethodName();

        if (methodName == null)
            methodName = "execute";

        String actionClazz = ProfilingUtils.getJustClassName(proxy.getConfig().getClassName());

        return "XW Interceptor: " + (location != null ? location + ": " : "") + proxy.getNamespace() + "/" + proxy.getActionName() + ".action (" + actionClazz + "." + methodName + "())";
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
