package com.atlassian.xwork.interceptors;

import com.opensymphony.util.TextUtils;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.ActionInvocation;
import com.opensymphony.xwork.ActionSupport;
import com.opensymphony.xwork.interceptor.DefaultWorkflowInterceptor;

public class CancelWorkflowInterceptor extends DefaultWorkflowInterceptor {
    public static final String CANCEL = "cancel";

    public String intercept(ActionInvocation actionInvocation) throws Exception {
        if (actionInvocation.getAction() instanceof ActionSupport) {
            if (TextUtils.stringSet(ServletActionContext.getRequest().getParameter("cancel")))
                return CANCEL;
        }

        return super.intercept(actionInvocation);
    }
}
