package com.atlassian.xwork;

import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.webwork.dispatcher.multipart.MultiPartRequestWrapper;
import org.apache.log4j.Category;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

public class FileUploadUtils {
    private static Category log = Category.getInstance(FileUploadUtils.class);

    public static File getSingleFile() throws FileUploadException {
        FileUploadUtils.UploadedFile uploadedFile = getSingleUploadedFile();
        return uploadedFile == null ? null : uploadedFile.getFile();
    }

    public static UploadedFile getSingleUploadedFile() throws FileUploadException {
        MultiPartRequestWrapper multiWrapper = (MultiPartRequestWrapper) ServletActionContext.getRequest();

        FileUploadUtils.UploadedFile[] uploadedFiles = FileUploadUtils.handleFileUpload(multiWrapper, true);

        if (uploadedFiles.length == 0)
            return null;

        return uploadedFiles[0];
    }

    /**
     * The multipart request should always be checked for errors before processing is done on it.
     *
     * @throws FileUploadException
     */
    public static void checkMultiPartRequestForErrors(MultiPartRequestWrapper multiWrapper) throws FileUploadException {
        if (multiWrapper.hasErrors()) {
            FileUploadException fileUploadException = new FileUploadException();
            Collection errors = multiWrapper.getErrors();
            Iterator i = errors.iterator();

            while (i.hasNext()) {
                String error = (String) i.next();
                log.error(error);
                fileUploadException.addError(error);
            }

            throw fileUploadException;
        }
    }


    public static UploadedFile[] handleFileUpload(MultiPartRequestWrapper multiWrapper, boolean clean)
            throws FileUploadException {
        checkMultiPartRequestForErrors(multiWrapper);

        Enumeration e = multiWrapper.getFileParameterNames();
        List uploadedFiles = new ArrayList();

        while (e.hasMoreElements()) {
            // get the value of this input tag
            String inputValue = (String) e.nextElement();

            // Get a File object for the uploaded File
            File[] files = multiWrapper.getFiles(inputValue);

            for (int i = 0; i < files.length; i++) // support multiple upload controls with the same name
            {
                File file = files[i];

                // If it's null the upload failed
                if (file == null && !clean) {
                    FileUploadException fileUploadException = new FileUploadException();
                    fileUploadException.addError("Error uploading: " + multiWrapper.getFileSystemNames(inputValue)[i]);
                    throw fileUploadException;
                } else if (file == null) {
                    continue; //i.e. we simply don't store nulls in thye uploadedFiles array.
                }

                UploadedFile uploadedFile = new UploadedFile(file, multiWrapper.getFileNames(inputValue)[i],
                        multiWrapper.getContentTypes(inputValue)[i]);
                uploadedFiles.add(uploadedFile);
            }
        }

        return (UploadedFile[]) uploadedFiles.toArray(new UploadedFile[0]);
    }

    public static final class UploadedFile {
        private File file;
        private String fileName;
        private String contentType;

        public UploadedFile(File file, String fileName, String contentType) {
            this.file = file;
            this.fileName = fileName;
            this.contentType = contentType;
        }

        public File getFile() {
            return file;
        }

        public String getFileName() {
            return fileName;
        }

        public String getContentType() {
            return contentType;
        }
    }

    public static final class FileUploadException extends Exception {
        private List errors = new ArrayList();

        public void addError(String error) {
            errors.add(error);
        }

        public String[] getErrors() {
            return (String[]) errors.toArray(new String[0]);
        }

        public String getMessage() {
            String s = "";
            String sep = "";
            for (Iterator i = errors.iterator(); i.hasNext(); ) {
                s += sep + i.next();
                sep = ", ";
            }
            return s;
        }
    }
}
