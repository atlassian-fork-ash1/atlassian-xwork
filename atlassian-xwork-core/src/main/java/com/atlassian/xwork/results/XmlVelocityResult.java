package com.atlassian.xwork.results;

/**
 * Sets the default content type to "text/xml" in order to serve xml only pages (like RSS)
 * <p>
 * You should create a result for this in xwork.xml, and add it to action configs that serve RSS views.
 */
public class XmlVelocityResult extends ProfiledVelocityResult {
    protected String getContentType(String s) {
        return "text/xml";
    }
}
