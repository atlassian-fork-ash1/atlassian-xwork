/**
 *
 */
package com.atlassian.xwork.validator.validators;

import com.opensymphony.util.TextUtils;
import com.opensymphony.xwork.validator.ValidationException;
import com.opensymphony.xwork.validator.validators.FieldValidatorSupport;

/**
 * Validate that a field contains comma separated e-mail addresses.
 *
 * @author Paul Curren
 */
public class CommaSeparatedEmailValidator extends FieldValidatorSupport {

    public void validate(Object object) throws ValidationException {
        String fieldName = getFieldName();
        String value = (String) getFieldValue(fieldName, object);

        if (value == null) {
            return;
        }

        value = value.trim();

        if (value.length() == 0) {
            return;
        }

        String[] emails = value.split("\\s*,\\s*"); // split on comma surrounded by option whitespace

        // now validate each e-mail address
        for (int i = 0; i < emails.length; i++) {
            if (!TextUtils.verifyEmail(emails[i])) {
                addFieldError(fieldName, object);
                break;
            }
        }
    }
}
