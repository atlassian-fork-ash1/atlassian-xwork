package com.atlassian.xwork;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Method level annotation for XWork actions to mark whether a particular action method invocation needs to be
 * protected by an XSRF token.
 *
 * @deprecated since 2.1. Use {@link com.atlassian.annotations.security.XsrfProtectionExcluded} instead.
 */
@Deprecated
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RequireSecurityToken {
    boolean value();
}
