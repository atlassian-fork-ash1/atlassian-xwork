package com.atlassian.xwork;

import com.opensymphony.webwork.config.Configuration;
import com.opensymphony.webwork.dispatcher.multipart.MultiPartRequest;
import http.utils.multipartrequest.ServletMultipartRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * Patched version of {@link com.opensymphony.webwork.dispatcher.multipart.PellMultiPartRequest}.
 * The original implementation would throw a {@link NullPointerException} if getFileNames() was called for
 * a non-existent fieldname.
 * <p>
 * The entire class had to be duplicated because of the private ServletMultipartRequest
 */
public class PellMultiPartRequest extends MultiPartRequest {
    //~ Instance fields ////////////////////////////////////////////////////////

    private ServletMultipartRequest multi;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     * Creates a new request wrapper to handle multi-part data using methods adapted from Jason Pell's
     * multipart classes (see class description).
     *
     * @param maxSize        maximum size post allowed
     * @param saveDir        the directory to save off the file
     * @param servletRequest the request containing the multipart
     */
    public PellMultiPartRequest(HttpServletRequest servletRequest, String saveDir, int maxSize) throws IOException {
        //this needs to be synchronised, as we should not change the encoding at the same time as
        //calling the constructor.  See javadoc for MultipartRequest.setEncoding().
        synchronized (this) {
            setEncoding();
            multi = new ServletMultipartRequest(servletRequest, saveDir, maxSize);
        }
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public Enumeration getFileParameterNames() {
        return multi.getFileParameterNames();
    }

    public String[] getContentType(String fieldName) {
        return new String[]{multi.getContentType(fieldName)};
    }

    public File[] getFile(String fieldName) {
        return new File[]{multi.getFile(fieldName)};
    }

    public String[] getFileNames(String fieldName) {
        // This is a bug fix from the com.opensymphony.webwork.dispatcher.multipart.PellMultiPartRequest
        // The original implementation did not check for a null return from multi.getFile()
        File file = multi.getFile(fieldName);
        if (file == null)
            return null;

        return new String[]{file.getName()};
    }

    public String[] getFilesystemName(String fieldName) {
        return new String[]{multi.getFileSystemName(fieldName)};
    }

    public String getParameter(String name) {
        return multi.getURLParameter(name);
    }

    public Enumeration getParameterNames() {
        return multi.getParameterNames();
    }

    public String[] getParameterValues(String name) {
        Enumeration enumeration = multi.getURLParameters(name);

        if (!enumeration.hasMoreElements()) {
            return null;
        }

        List values = new ArrayList();

        while (enumeration.hasMoreElements()) {
            values.add(enumeration.nextElement());
        }

        return (String[]) values.toArray(new String[values.size()]);
    }

    /**
     * Sets the encoding for the uploaded params.  This needs to be set if you are using character sets other than
     * ASCII.
     * <p/>
     * The encoding is looked up from the configuration setting 'webwork.i18n.encoding'.  This is usually set in
     * default.properties & webwork.properties.
     */
    private static void setEncoding() {
        String encoding = null;

        try {
            encoding = Configuration.getString("webwork.i18n.encoding");

            if (encoding != null) {
                //NB: This should never be called at the same time as the constructor for
                //ServletMultiPartRequest, as it can cause problems.
                //See javadoc for MultipartRequest.setEncoding()
                http.utils.multipartrequest.MultipartRequest.setEncoding(encoding);
            } else {
                http.utils.multipartrequest.MultipartRequest.setEncoding("UTF-8");
            }
        } catch (IllegalArgumentException e) {
            log.info("Could not get encoding property 'webwork.i18n.encoding' for file upload.  Using system default");
        } catch (UnsupportedEncodingException e) {
            log.error("Encoding " + encoding + " is not a valid encoding.  Please check your webwork.properties file.");
        }
    }
}

