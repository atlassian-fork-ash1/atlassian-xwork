package com.atlassian.xwork10.interceptors;

import com.atlassian.xwork10.Xwork10VersionSupport;

/**
 * //TODO define class
 */
public class SafeParametersInterceptor extends com.atlassian.xwork.interceptors.SafeParametersInterceptor {
    public SafeParametersInterceptor() {
        super(new Xwork10VersionSupport());
    }
}
