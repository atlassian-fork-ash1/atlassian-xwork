package com.atlassian.xwork12;

import com.atlassian.xwork.XWorkVersionSupport;
import com.opensymphony.xwork.Action;
import com.opensymphony.xwork.ActionInvocation;

import java.lang.reflect.Method;

/**
 * Wrapper for functions specific to XWork 1.2
 */
public class Xwork12VersionSupport implements XWorkVersionSupport {
    public Action extractAction(ActionInvocation invocation) {
        return (Action) invocation.getAction();
    }

    public Method extractMethod(ActionInvocation invocation) throws NoSuchMethodException {
        final Class<?> actionClass = invocation.getAction().getClass();
        final String methodName = invocation.getProxy().getMethod();

        try {
            return actionClass.getMethod(methodName);
        } catch (NoSuchMethodException e) {
            try {
                String altMethodName = "do" + methodName.substring(0, 1).toUpperCase() + methodName.substring(1);
                return actionClass.getMethod(altMethodName);
            } catch (NoSuchMethodException e1) {
                // throw the original one
                throw e;
            }
        }
    }
}
