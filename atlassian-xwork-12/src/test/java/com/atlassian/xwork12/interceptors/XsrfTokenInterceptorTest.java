package com.atlassian.xwork12.interceptors;

import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.config.entities.ActionConfig;
import com.opensymphony.xwork.mock.MockActionInvocation;
import com.opensymphony.xwork.mock.MockActionProxy;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

import javax.servlet.http.HttpServletRequest;

public class XsrfTokenInterceptorTest extends MockObjectTestCase {
    private Mock mockServletRequest;

    private XsrfTokenInterceptor interceptor;

    @Override
    public void setUp() throws Exception {
        mockServletRequest = mock(HttpServletRequest.class);
        mockServletRequest.stubs().method("getHeader").with(eq(com.atlassian.xwork.interceptors.XsrfTokenInterceptor.OVERRIDE_HEADER_NAME)).will(returnValue(null));
        mockServletRequest.stubs().method("getParameter").with(eq(com.atlassian.xwork.interceptors.XsrfTokenInterceptor.REQUEST_PARAM_NAME)).will(returnValue(null));
        ServletActionContext.setRequest((HttpServletRequest) mockServletRequest.proxy());

        interceptor = new XsrfTokenInterceptor();
    }

    public void testInterceptInvokesNonProtectedAction() throws Exception {
        final MockActionProxy actionProxy = new MockActionProxy();
        actionProxy.setMethod("toString");
        actionProxy.setConfig(new ActionConfig());

        final MockActionInvocation actionInvocation = new MockActionInvocation();
        actionInvocation.setProxy(actionProxy);
        actionInvocation.setAction(new Object());
        actionInvocation.setResultCode("passed");
        mockServletRequest.expects(once()).method("getMethod").will(returnValue("GET"));

        assertEquals("passed", interceptor.intercept(actionInvocation));
    }
}
